FRONTEND_IMAGE ?= registry.gitlab.com/lyamtsev.s2017/swim-print-frontend
BACKEND_IMAGE ?= registry.gitlab.com/lyamtsev.s2017/swim-print-backend

update:
	docker pull $(FRONTEND_IMAGE) && \
	docker pull $(BACKEND_IMAGE)

run: update
	docker-compose up -d

clean:
	docker exec -it swim-print-production_db_1 bash -c "psql -U postgres postgres --command='DROP SCHEMA public CASCADE; \
	CREATE SCHEMA public; GRANT ALL ON SCHEMA public TO postgres; GRANT ALL ON SCHEMA public TO postgres; \
	GRANT ALL ON SCHEMA public TO public;'" && \
	echo !!!DATABASE SUCCESFULLY CLEANED, PLEASE RESTART SERVER!!!